"""
    Obfuscate DICOM files for client confidentiality purposes
    by Julian Rachele 20-Oct-2020
"""
import errno
import os

"""
    Example YAML configuration file:
    
    io:
      input_directory: .
      output_directory: ./anonymous
      suffix: _anon
    remove_private_fields: true
    fields:
      PatientName: anonymous
      PatientID: anonymous
"""


from pydicom import dcmread
import glob
import yaml

config_path = input("Path to config(yml): ")
with open(config_path) as c:
    config = yaml.safe_load(c)
    if "io" not in config:
        print("Must specify input/output directories in config with io rule")
        exit(1)
    if "fields" not in config:
        print("No fields to anonymize")
        exit(1)
    input_directory = config["io"]["input_directory"]
    output_directory = config["io"]["output_directory"]
    if input_directory is None or output_directory is None:
        print("Must specify input/output directories in config with io rule")
        exit(1)
    suffix = config["io"]["suffix"]
    wildcard = config["io"]["wildcard"]
    remove_private_fields = config["remove_private_fields"]
    if suffix is None:
        suffix = ""
    if wildcard is None:
        wildcard = "*.dcm"

    # Create output directory first
    try:
        os.makedirs(output_directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    # Iterate through DICOM files and apply anonymizations
    for d in glob.iglob(input_directory + "/" + wildcard):
        fpath = os.path.join(os.getcwd(), d)
        with open(fpath, 'rb') as infile:
            ds = dcmread(infile)
            if remove_private_fields: ds.remove_private_tags()
            for field in config["fields"]:
                if field not in config["fields"]:
                    print("Unable to find field {} in DCM file {}".format(field, d))
                    exit(1)
                ds[field].value = config["fields"][field]
            fname = d[2:-4] + suffix + ".dcm"
            out = output_directory + "/" + fname
            print("Writing: " + out)
            ds.save_as(out)
