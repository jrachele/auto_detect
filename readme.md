# CT Auto Detection

This tool attempts to track the aorta through a CT scan of an adult human body, starting from
mid-chest and continuing through the feet.

The program works first by the user supplying two .TIF files, which represent aggregate
CT scan data from one person.

The first file is simply the original .TIF file created following the CT scan. The second
file is a "binary" file, essentially a black and white .TIF, which aids the tool in detecting
the aorta.

## Creation of the binary .TIF file

The binary file is created using a .TIF image manipulation tool, such as
[ImageJ](https://imagej.nih.gov/ij/). The goal is to mask out the regions that are dark-grey
to black. What will be left behind is bone, dense tissues, and most of all, an insular oval, which is our aorta.

To do this in ImageJ, follow these steps:

1. Open the original .TIF file in ImageJ
1. Go to Image -> Adjust -> Threshold
1. On this screen, ensure that the Color Mode is changed from Red to B&W, and that *Stack histogram* is enabled
1. For *6572_Binary.tif*, which comes bundled with this software, the values used to isolate the
ovular aorta were -1024 and 165. 
1. Once the threshold is applied, save the file as a .TIF and proceed.

## Running the software

This software package uses Python 3, and depends on the following dependencies:

To install all the necessary dependencies, run the following command:

```pip3 install numpy opencv-python imageio exifread tifffile --user```

Then you may run the program by issuing the command ```python3 main.py```

## Pixel values

Generally, the lumen of the aorta has a pixel range of roughly 261 to 338, a vaguely light-grey color range

Calcifications, as they are reflective boney tissue, will be bright white and represent an upper bound of about 1300.

Then, the dark area around the aorta, which is the lower bound of our pixel range, is at max a value of 100. 

Therefore, the default range is 100-1300.



