import imageio
import tensorflow as tf
import numpy as np
import cv2
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow.keras.backend as kb

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras import Model, Input
from tensorflow.keras.preprocessing.image import ImageDataGenerator
# from tensorflow_core.python.keras import Input, Model
from tensorflow_core.python.keras.layers import Reshape, Lambda, ThresholdedReLU, UpSampling2D, concatenate
from tensorflow_core.python.keras.optimizer_v2.adam import Adam

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)

original_file="6572_Original.tif"
_, original_stack = cv2.imreadmulti(original_file, mats=[], flags=cv2.IMREAD_UNCHANGED)

binary_file="6572_Isolated.tif"
_, binary_stack = cv2.imreadmulti(binary_file, mats=[], flags=cv2.IMREAD_UNCHANGED)

REAL_SIZE = 512
WINDOW_SIZE = 256
edge_size = (REAL_SIZE // 2) - (WINDOW_SIZE // 2)
original_stack = [frame[edge_size:-edge_size, edge_size:-edge_size] / 4096 for frame in original_stack]
binary_stack = [frame[edge_size:-edge_size, edge_size:-edge_size] / 255 for frame in binary_stack]


limit = 200
x_train = tf.cast(np.asarray(original_stack[:limit]), tf.float32)
y_train = tf.cast(np.asarray(binary_stack[:limit]), tf.float32)


def unet(pretrained_weights=None, input_size=(WINDOW_SIZE, WINDOW_SIZE)):
    inputs = Input(input_size)
    resized = Reshape((WINDOW_SIZE, WINDOW_SIZE, 1))(inputs)
    conv1 = Conv2D(32, 3, activation='relu', padding='same', kernel_initializer='he_normal')(resized)
    conv1 = Conv2D(32, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
    conv2 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
    conv3 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool3)
    conv4 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool4)
    conv5 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
    drop5 = Dropout(0.5)(conv5)

    up6 = Conv2D(256, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(drop5))
    merge6 = concatenate([drop4, up6], axis=3)
    conv6 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge6)
    conv6 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv6)

    up7 = Conv2D(128, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv6))
    merge7 = concatenate([conv3, up7], axis=3)
    conv7 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge7)
    conv7 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv7)

    up8 = Conv2D(64, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv7))
    merge8 = concatenate([conv2, up8], axis=3)
    conv8 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge8)
    conv8 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)

    up9 = Conv2D(32, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv8))
    merge9 = concatenate([conv1, up9], axis=3)
    conv9 = Conv2D(32, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge9)
    conv9 = Conv2D(32, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv9 = Conv2D(2, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv10 = Conv2D(1, 1, activation='sigmoid')(conv9)

    model = Model(inputs=inputs, outputs=conv10)

    model.compile(optimizer=Adam(lr=1e-4), loss='binary_crossentropy', metrics=['accuracy'])

    # model.summary()

    if (pretrained_weights):
        model.load_weights(pretrained_weights)

    return model


# def smaller_net(input_size=(128, 128)):
#     inputs = Input(shape=input_size)
#     resized = Reshape((128, 128, 1))(inputs)
#     conv1 = Conv2D(32, 3, activation='relu', padding='same', kernel_initializer='he_normal')(resized)
#     pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
#     conv2 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
#     drop2 = Dropout(0.5)(conv2)
#     pool2 = MaxPooling2D(pool_size=(2, 2))(drop2)
#     conv3 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
#     drop3 = Dropout(0.5)(conv3)
#     up3 = UpSampling2D((2, 2))(drop3)
#     merge3 = concatenate([drop2, up3], axis=3)
#     conv4 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge3)
#     up4 = UpSampling2D((2, 2))(conv4)
#     merge4 = concatenate([conv1, up4], axis=3)
#     conv5 = Conv2D(32, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge4)
#     # conv5 = Conv2D(2, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
#     conv6 = Conv2D(1, 1, activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
#
#     model = Model(inputs=inputs, outputs=conv6)
#
#     model.compile(optimizer=Adam(lr=1e-2), loss='MSE', metrics=['accuracy'])
#     return model

# model = smaller_net()
model = unet()

print(model.summary())
checkpoint_dir = "training_checkpoint"
save_training = tf.keras.callbacks.ModelCheckpoint(filepath="training_checkpoint/cp.cpkt", save_weights_only=1, verbose=1)
latest = tf.train.latest_checkpoint(checkpoint_dir)
if latest is not None:
    print("Resuming training")
    model.load_weights(latest)
model.fit(x_train, y_train, epochs=100, callbacks=[save_training])
# model.fit(x_train, y_train, epochs=50)


def save_final_model(prediction, path):
    imageio.mimwrite(path, prediction)
#
# test_file="6572_Original.tif"
# _, original_stack = cv2.imreadmulti(original_file, mats=[], flags=cv2.IMREAD_UNCHANGED)
# x_train = tf.cast(np.asarray(original_stack), tf.float32) / 255.0
#


prediction = model.predict(x_train)
# save_final_model(original_stack, "6572_Original_Input.tif")
# save_final_model(binary_stack, "6572_Binary_Input.tif")
save_final_model(prediction*255, "6572_Test.tif")
# #
# cv2.imshow("Result", prediction[30])
# cv2.imshow("Actual", binary_stack[30])
#
# cv2.waitKey()
#


