import cv2
import numpy as np


def calculateOverlap(binary_image, overlap_type):
    def overlap(contours_a, contours_b, width, height):
        mask_a = np.zeros((width, height, 1), dtype=np.uint8)
        mask_b = np.zeros((width, height, 1), dtype=np.uint8)
        mask_a = cv2.drawContours(mask_a, [contours_a], -1, (255, 255, 255), -1)
        mask_b = cv2.drawContours(mask_b, [contours_b], -1, (255, 255, 255), -1)
        intersection = cv2.bitwise_and(mask_a, mask_b)
        if np.amax(intersection) == 255:
            return True
        return False

    def filterContour(contour):
        contour_area = cv2.contourArea(contour)
        if contour_area > 1400:
            return False
        if contour_area < 2:
            return False
        # Filter by convexity
        minConvexity = 0.5
        hull = cv2.convexHull(contour)
        hullArea = cv2.contourArea(hull)
        if not hullArea > 0:
            return False
        ratio = contour_area / hullArea
        if ratio < minConvexity:
            return False
        return True

    def setVesselParams(params):
        # Change thresholds
        params.minThreshold = 0
        params.maxThreshold = 255

        # Filter by area
        params.filterByArea = True
        params.minArea = 200
        params.maxArea = 1400

        # Filter by circularity
        params.filterByCircularity = True
        params.minCircularity = 0.45  # Might want to try turning this off if it doesn't detect anything

        # Filter by convexity
        params.filterByConvexity = True
        params.minConvexity = 0.8

        # Filter by inertia. Small inertia = line shape, large inertia = circle shape
        params.filterByInertia = True
        params.minInertiaRatio = 0.3

        params.filterByColor = True
        params.blobColor = 255


    
    w, h = binary_image[0].shape  # Width, height, and channels
    n = binary_image.__len__()  # Number of slices
    mask_final = binary_image[:]  #copy()
    contour_list = [[] for _ in range(n)]

    if overlap_type == 'vessel':
        # Use OpenCV's "blob detector" to detect the aorta
        initial_params = cv2.SimpleBlobDetector_Params()

        # Sets up default parameters, and mutates initial_params
        setVesselParams(initial_params)

        # Create detector
        detector = cv2.SimpleBlobDetector_create(initial_params)

        # Detect initial aorta
        keypoints = detector.detect(binary_image[0])

        # Make mask of initial aorta and get contours from it
        mask_init = np.zeros((w, h), dtype=np.uint8)
        if keypoints:
            for x in range(0, keypoints.__len__()):
                mask_init = cv2.circle(mask_init, (np.int(keypoints[x].pt[0]), np.int(keypoints[x].pt[1])),
                                       radius=np.int(keypoints[x].size / 2 + 3), color=(255, 255, 255),
                                       thickness=-1)
            mask_init = cv2.bitwise_and(binary_image[0], binary_image[0], mask=mask_init)
    else:
        mask_init = binary_image[0]

    contours, hierarchy = cv2.findContours(mask_init, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    # Loop through and find overlapping blobs
    for i in range(n):
        checked = set()

        mask = np.zeros((w, h), dtype=np.uint8)

        # Look for new contours
        contours_new, hierarchy_new = cv2.findContours(binary_image[i], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        contours_temp = []

        # Check for overlap
        for x in range(0, contours_new.__len__()):
            for y in range(0, contours.__len__()):
                if x not in checked and filterContour(contours_new[x]) and overlap(contours[y], contours_new[x], w, h):
                    contours_temp.append(contours_new[x])
                    checked.add(x)

        # Contours added to list become new array to check against
        contours = contours_temp
        contour_list[i] = contours

        # Draw current contours array
        cv2.drawContours(mask, contours, -1, (255, 255, 255), -1)
        mask_final[i] = mask

    return mask_final, contour_list
